<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button Account Profile</name>
   <tag></tag>
   <elementGuidId>381fca19-959b-4004-9f83-d2dc7b9bbf5c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#sso__icon__login_top</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//i[@id='sso__icon__login_top']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>b357f477-c8ac-4c8f-92af-641a3e185ab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sso__icon__login</value>
      <webElementGuid>446844ad-7223-4213-9f7c-7c72e109c636</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>sso__icon__login_top</value>
      <webElementGuid>b0f5ea1e-094a-4787-91d6-320b5ab813e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sso__icon__login_top&quot;)</value>
      <webElementGuid>b4efe91e-da18-4b45-9389-9bc5548021ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//i[@id='sso__icon__login_top']</value>
      <webElementGuid>40e7d9b1-f52d-48f7-a595-f2dd7c8df48a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a/i</value>
      <webElementGuid>be550021-0001-4d67-aba6-5612e0fa459e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//i[@id = 'sso__icon__login_top']</value>
      <webElementGuid>d8ee018c-519f-4602-ad41-b6c664968b86</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
