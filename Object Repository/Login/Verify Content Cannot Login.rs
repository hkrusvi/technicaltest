<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Verify Content Cannot Login</name>
   <tag></tag>
   <elementGuidId>90559f7c-5c72-4594-ae3e-ea1156653ea3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.form-error.text-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b5960872-90fa-4a95-a7c9-bda9d90a80cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-error text-center</value>
      <webElementGuid>5d410b12-2ba6-49ff-b5e0-09a2eff2932a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            KG Media ID atau password yang kamu masukkan salah
        </value>
      <webElementGuid>cecc2810-f465-460d-8191-7dea02869c36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;container&quot;]/div[@class=&quot;block-middle&quot;]/div[@class=&quot;form-main&quot;]/form[1]/div[@class=&quot;form-error text-center&quot;]</value>
      <webElementGuid>9602f192-977f-4a05-a071-fb55717cf9d8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::div[1]</value>
      <webElementGuid>d66e13b5-d2cf-4682-81f1-b927a95711ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='KG Media ID'])[2]/following::div[2]</value>
      <webElementGuid>00aa7b2f-a96e-4190-be63-7c3b2f3168d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar KG Media ID?'])[1]/preceding::div[2]</value>
      <webElementGuid>187f89b0-119e-49a6-b658-01fb890288d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='lupa password?'])[1]/preceding::div[2]</value>
      <webElementGuid>1dc329d7-73bf-45a3-889f-966d8a965dce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='KG Media ID atau password yang kamu masukkan salah']/parent::*</value>
      <webElementGuid>35e4428a-a49f-4ba2-86a6-64b623d0d2e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[3]</value>
      <webElementGuid>2ecc0716-d8e8-4750-aa6d-ab71bffd91ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            KG Media ID atau password yang kamu masukkan salah
        ' or . = '
            KG Media ID atau password yang kamu masukkan salah
        ')]</value>
      <webElementGuid>df35d515-9712-4393-abc9-5eb060a7cd8b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
