<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Verify Content Email</name>
   <tag></tag>
   <elementGuidId>f3146948-d56d-4ff9-8c7f-a12d6d20c395</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='KG Media ID'])[1]/following::label[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>ee00ee23-cdbb-41e4-bd5e-3f8a0117e73b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>c047d9f3-b56b-409e-bf86-45990865da87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>KG Media ID</value>
      <webElementGuid>ab6e85f1-99e7-49fc-85f2-03ecfc781e94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;container&quot;]/div[@class=&quot;block-middle&quot;]/div[@class=&quot;form-main&quot;]/form[1]/div[@class=&quot;form-row form-input&quot;]/label[1]</value>
      <webElementGuid>9e8fda6f-e13f-4b85-85a6-0cf397d49dff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='KG Media ID'])[1]/following::label[1]</value>
      <webElementGuid>e726a3f1-1077-4393-bddb-18bd9f573aaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/preceding::label[1]</value>
      <webElementGuid>0908a934-2c57-40f4-a2a3-112ef153e9b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar KG Media ID?'])[1]/preceding::label[2]</value>
      <webElementGuid>8598be32-964f-480a-9c6b-14914bbfbe43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label</value>
      <webElementGuid>99a2d1d4-a727-4ef9-b030-9c38c53ea789</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'KG Media ID' or . = 'KG Media ID')]</value>
      <webElementGuid>c9b5cf2f-fda7-446a-933e-c7ee4023106a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
