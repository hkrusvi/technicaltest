<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Verify Content News Title</name>
   <tag></tag>
   <elementGuidId>2507e2ca-c04e-4061-b899-a202bc92ed2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.read__title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Regional'])[2]/following::h1[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'read__title']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>e53829fb-b434-44da-a303-ad469ff3fcf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>read__title</value>
      <webElementGuid>af6943b7-1053-48bf-b468-0a3289deb6e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sindikat Jual Beli Mobil Bodong di Pati dan Jepara Terbongkar</value>
      <webElementGuid>5d7be918-4e23-4d73-a599-2e97b0d107e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js csstransitions&quot;]/body[@class=&quot;theme--news page--read&quot;]/div[@class=&quot;wrap&quot;]/div[@class=&quot;container clearfix&quot;]/div[@class=&quot;row mt2 col-offset-fluid clearfix&quot;]/div[@class=&quot;col-bs10-10&quot;]/h1[@class=&quot;read__title&quot;]</value>
      <webElementGuid>54f0baf8-c09c-4fbd-8f3f-c56bf72b093d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Regional'])[2]/following::h1[1]</value>
      <webElementGuid>dc026d31-443b-45de-b7b4-ce83f30a2446</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kompas.com'])[1]/following::h1[1]</value>
      <webElementGuid>742c8ba6-78fb-46e4-95d0-79cbf371bb99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kompas.com'])[2]/preceding::h1[1]</value>
      <webElementGuid>b470d383-2ef1-4a12-9d09-0e1611856788</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>03ad07f0-4357-4cf0-bb6e-5c6cfadbeb13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Sindikat Jual Beli Mobil Bodong di Pati dan Jepara Terbongkar' or . = 'Sindikat Jual Beli Mobil Bodong di Pati dan Jepara Terbongkar')]</value>
      <webElementGuid>5689a0f3-6fe2-4527-81bb-0b7d10a59899</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
